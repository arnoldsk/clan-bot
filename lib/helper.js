
module.exports = (config) => {
    return {
        emoji: {
            mail: '📧',
            cross: '❌',
            check: '✅',
            new: '🆕',
            door: '🚪',
            mag: '🔍',
            moneybag: '💰',
            warning: '⚠',
        },

        randomColor: () => {
            return Math.floor(Math.random() * 16777214) + 1;
        },

        /**
         * https://discordapp.com/developers/docs/topics/permissions
         */
        memberHasPerms: (member, additional) => {
            if (config.ownerIds.indexOf(member.id) != -1) {
                return true;
            }

            // Some permissions that I think are enough to prove high permissions
            return member.hasPermission('ADMINISTRATOR') ||
                member.hasPermission('KICK_MEMBERS') ||
                member.hasPermission('BAN_MEMBERS');
        },

        checkClientGuildPermissions: (client, guild, onPassCallback) => {
            guild.fetchMember(client.user).then((member) => {
                if (!(
                    member.hasPermission('ADMINISTRATOR') &&
                    member.hasPermission('MANAGE_CHANNELS') &&
                    member.hasPermission('MANAGE_MESSAGES') &&
                    member.hasPermission('MANAGE_ROLES')
                )) {
                    let msg = `I lack permissions to operate in ${guild.name}, bye again.`;
                    guild.owner.sendMessage(msg).then(() => {
                        console.warning(`Leaving guild ${guild.name} because of lack of permissions.`);
                        guild.leave();
                    });
                } else if (typeof onPassCallback === 'function') {
                    onPassCallback(client, guild);
                }
            });
        },

        getClanChannelOwerwrites: (guild, role) => {
            const everyoneRole = guild.roles.first();

            return [
                { // Disallow everyone to see
                    type: 'role',
                    id: everyoneRole.id,
                    deny: ['READ_MESSAGES', 'CREATE_INSTANT_INVITE'],
                },
                { // Allow clan members to see
                    type: 'role',
                    id: role.id,
                    deny: ['CREATE_INSTANT_INVITE'],
                    allow: ['READ_MESSAGES'],
                },
            ];
        }
    }
};

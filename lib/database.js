
const mysql = require('mysql2/promise');

class Database {
    constructor(config) {
        this.config = config;
        this._instance = null;
    }

    async get() {
        if (!this._instance) {
            this._instance = await mysql.createConnection(this.config.db);
        }

        return this._instance;
    }
}

module.exports = (...args) => new Database(...args);

/**
 * Config
 */
const config = require('./config');

/**
 * Dependencies and libraries
 */
const Discord = require('discord.js');
const fs = require('fs');
const helper = require('./lib/helper')(config);
const database = require('./lib/database')(config);

/**
 * Discord bot client
 */
const client = new Discord.Client();

/**
 * On client ready event
 */
client.on('ready', () => {
    console.info('\n', `${client.user.username} is ready for action!`);

    client.user.setActivity(`${config.prefix} help`, { type: 'LISTENING' });

    for (const guild of client.guilds.array()) {
        helper.checkClientGuildPermissions(client, guild, () => {
            console.info(`- ${guild.name} with ${guild.memberCount} members.`, '\n');
        });
    }
});

/**
 * On message sent event
 */
client.on('message', async (msg) => {
    /**
     * Validate that the message is sent by a human user
     */
    if (msg.author.bot || !msg.member) {
        return;
    }

    /**
     * Ignore anything that doesn't start with the prefix
     */
    if (msg.content.toLowerCase().indexOf(config.prefix) !== 0) return;

    /**
     * Extract command and arguments
     */
    const args = msg.content.slice(config.prefix.length).trim().split(/\s+/g);
    const command = args.shift().toLowerCase();

    /**
     * Find and execute the command
     */
    if (fs.existsSync(`./commands/${command}.js`)) {
        const Command = require(`./commands/${command}`);

        // Disallow non-admins to call admin commands
        if (Command.admin && !helper.memberHasPerms(msg.member)) {
            msg.react(helper.emoji.cross);
            return;
        }

        Command.function(msg, args, {
            command,
            client,
            config,
            helper,
            fs,
            database,
        });
    }
});

/**
 * Authenticate/Run the bot
 */
client.login(config.token);


const command = async (msg, args, data) => {
    const helper = data.helper;

    /**
     * Get all clan names
     * TODO: in the furute do not just assume that categories are clan names
     */
    const clans = msg.guild.channels.findAll('type', 'category');

    /**
     * If none found
     */
    if (!clans.length) {
        msg.channel.send(`There aren't any clans yet.`);
        return;
    }

    /**
     * Otherwise display all names
     */
    const fields = [];
    for (const clan of clans) {
        fields.push({
            name: clan.name,
            value: `With ${clan.children.size} channels`,
            inline: true,
        });
    }

    msg.member.send({
        message: true,
        embed: {
            fields,
        }
    });

    msg.react(helper.emoji.mail);
};

module.exports = {
    function: command,
    info: 'List all clan names.',
    options: null,
    admin: false,
};

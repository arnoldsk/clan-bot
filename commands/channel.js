
const command = async (msg, args, data) => {
    const config = data.config;
    const helper = data.helper;
    const fs = data.fs;

    // Fetch requested command filename
    const filename = args[0] || null;

    // Shift args
    args = args.slice(1);

    // Execute the new command if found
    if (fs.existsSync(`${__dirname}/channel/${filename}.js`)) {
        require(`${__dirname}/channel/${filename}`)(msg, args, data);
    }
};

module.exports = {
    function: command,
    info: 'Manage clan channels.',
    options: ['add | delete'],
    admin: true, // For now at least
};


const command = async (msg, args, data) => {
    const helper = data.helper;

    /**
     * Verify that is being called from a clan category
     */
    if (!msg.channel.parent || msg.channel.parent.type !== 'category') {
        msg.react(helper.emoji.cross);
        return;
    }

    const clanName = msg.channel.parent.name;

    // TODO: Here comes the part where I need the clan names stored to verify that this is a clan

    /**
     * Verify that the clan memeber is trying to leave it
     */
    const role = msg.member.roles.find('name', clanName);

    if (!role) {
        msg.react(helper.emoji.cross);
        return;
    }

    /**
     * Remove the clan role from the member
     */
    msg.member.removeRole(role);

    msg.react(helper.emoji.check);

    /**
     * Send a message just in case he didn't want to do this
     * So he can find where did he leave from
     */
    msg.member.send(`You left from the clan "${clanName}".`, {
        message: true,
    });
};

module.exports = {
    function: command,
    info: 'Leave from a clan.',
    options: null,
    admin: false,
};


const command = async (msg, args, data) => {
    const helper = data.helper;

    // TODO store clan names somewhere

    /**
     * Get clan name from arguments
     */
    const name = args.join(' ').trim();

    if (!name || !name.length) {
        msg.react(helper.emoji.cross);
        return;
    }

    /**
     * Check if role doesn't already exist
     */
    const roleExists = msg.guild.roles.some((r) => r.name.toLowerCase() == name.toLowerCase());

    if (roleExists) {
        msg.react(helper.emoji.cross);
        return;
    }

    /**
     * Create clan's role
     */
    const role = await msg.guild.createRole({
        name,
    });

    /**
     * Assign the role to the person that created the clan
     */
    msg.member.addRole(role);

    /**
     * Create clan category and general text and voice channel
     */
    const category = await msg.guild.createChannel(name, 'category', helper.getClanChannelOwerwrites(msg.guild, role));

    const textChannel = await msg.guild.createChannel('general', 'text', helper.getClanChannelOwerwrites(msg.guild, role));
    textChannel.setParent(category);

    const voiceChannel = await msg.guild.createChannel('general', 'voice', helper.getClanChannelOwerwrites(msg.guild, role));
    voiceChannel.setParent(category);

    textChannel.send(`What would it take to conquer the world?`, {
        mention: msg.member,
    });

    msg.react(helper.emoji.check);
};

module.exports = {
    function: command,
    info: 'Create a new clan.',
    options: null,
    admin: true, // For now at least
};

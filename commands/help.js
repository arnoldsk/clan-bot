
const command = (msg, args, data) => {
    const helper = data.helper;
    const fs = data.fs;

    const fields = [];
    const files = fs.readdirSync(__dirname).filter((file) => file.substr(-3) === '.js');

    for (const file of files) {
        const name = file.substr(0, file.length - 3);
        const command = require(`${__dirname}/${file}`);

        const adminPrefix = command.admin ? '**[admin]**' : '';
        const options = command.options
            ? ('<' + command.options.join('> <') + '>')
            : '';

        fields.push({
            name: `${data.config.prefix} ${name} ${options}`,
            value: `${adminPrefix} ${command.info}`,
        });
    }

    msg.member.send({
        message: true,
        embed: {
            fields,
        }
    });

    msg.react(helper.emoji.mail);
};

module.exports = {
    function: command,
    info: 'Display information about commands.',
    options: null,
    admin: false,
};

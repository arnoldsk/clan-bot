
const command = async (msg, args, data) => {
    const helper = data.helper;

    // TODO I need to store names for this
    // For now I'll assume the clan names are categories

    /**
     * Get clan name from arguments
     */
    const name = args.join(' ').trim();

    if (!name || !name.length) {
        msg.react(helper.emoji.cross);
        return;
    }

    /**
     * Get all clan names
     * TODO: in the furute do not just assume that categories are clan names
     */
    const clans = msg.guild.channels.findAll('type', 'category');

    /**
     * Filter for the clan name
     */
    const clan = clans.filter((value) => value.name === name)[0];

    if (!clan) {
        msg.react(helper.emoji.cross);
        return;
    }

    /**
     * Find the role
     */
    const role = msg.guild.roles.find('name', clan.name);

    if (!role) {
        msg.react(helper.emoji.cross);
        return;
    }

    /**
     * Add the role to the user, that's it
     */
    msg.member.addRole(role);

    msg.react(helper.emoji.check);
};

module.exports = {
    function: command,
    info: 'Join a clan.',
    options: null,
    admin: false,
};


const command = async (msg, args, data) => {
    const helper = data.helper;

    /**
     * Get clan name from arguments
     */
    const name = args.join(' ').trim();

    if (!name || !name.length) {
        msg.react(helper.emoji.cross);
        return;
    }

    /**
     * Fetch the role based on the name
     */
    const role = msg.guild.roles.find('name', name);

    if (!role) {
        msg.react(helper.emoji.cross);
        return;
    }

    /**
     * Delete the role
     */
    await role.delete();

    /**
     * Find the role category, remove all child categories and delete self
     */
    const category = msg.guild.channels.find((value) => value.name == name && value.type == 'category');

    if (!category) {
        msg.react(helper.emoji.cross);
        return;
    }

    for (const channel of category.children.array()) {
        // Wait until it's deleted so there aren't any ghost clan channels left
        await channel.delete();
    }

    // Delete the channel itself
    category.delete();

    msg.react(helper.emoji.check);
};

module.exports = {
    function: command,
    info: 'Remove a clan by name.',
    options: null,
    admin: true, // For now at least
};

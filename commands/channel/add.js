
module.exports = async (msg, args, data) => {
    const helper = data.helper;

    /**
     * Verify that is being called from a clan category
     */
    if (!msg.channel.parent || msg.channel.parent.type !== 'category') {
        msg.react(helper.emoji.cross);
        return;
    }

    const category = msg.channel.parent;
    const clanName = category.name;

    // TODO: Here comes the part where I need the clan names stored to verify that this is a clan

    /**
     * Verify that the clan memeber is adding it
     */
    const role = msg.member.roles.find('name', clanName);

    if (!role) {
        msg.react(helper.emoji.cross);
        return;
    }

    /**
     * Get channel name from arguments
     */
    const name = args.join(' ').trim();

    const channel = await msg.guild.createChannel(name, 'text', helper.getClanChannelOwerwrites(msg.guild, role));
    channel.setParent(category);

    msg.react(helper.emoji.check);
};
